<html>
<head>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript" src="https://www.google.com/jsapi"></script>
    <script type="text/javascript" src="js/jquery-3.2.0.js"></script>
    <script type="text/javascript">
        google.charts.load("visualization", "1", {packages:["corechart"]});
        var timerId = setInterval(function(){
            $.ajax({
                type: "GET",
                url: "http://carcarych.azurewebsites.net/AzureApp/api",
                contentType: "text/plain",
                dataType: "text",
                success: function (data) {
                    var intArray = data.substring(1,data.length-1).split(",").map(Number);
                    var table = new google.visualization.DataTable();
                    table.addColumn('number', 'Time');
                    table.addColumn('number', 'Value');
                    $.each(intArray, function (index, value) {
                        table.addRow([index, value]);
                    });
                    var options = {
                        title: 'How Often Do You Blink?',
                        width: 900, height: 500,
                        legend: 'none',
                        curveType: 'function',
                        titleTextStyle: {
                            fontName: 'Times New Roman',
                            fontSize: 18,
                            bold: true,
                            italic: false
                        }
                    };
                    $(".load").remove();
                    var chart = new google.visualization.LineChart(document.getElementById('chart_div'));
                    chart.draw(table, options);
                }
            });},2000);

    </script>
    <style>
        .load {
            position: absolute;
            margin: auto;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
        .chart {
            margin: auto;
            width: 900px;
            height: 500px;
        }
    </style>
</head>

<body>
<div  id="chart_div" class="chart">
    <img id="img" src="resources/ajax-loader.gif" class="load"/>
</div>
</body>
</html>
