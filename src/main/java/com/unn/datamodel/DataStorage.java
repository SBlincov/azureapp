package com.unn.datamodel;

import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "data")
public class DataStorage implements Serializable {
    private List<Integer> list;

    public DataStorage() {
    }

    public DataStorage(List<Integer> list) {
        this.list = list;
    }

    public List<Integer> getList() {
        return list;
    }

    public void setList(List<Integer> list) {
        this.list = list;
    }

    @Override
    public String toString() {
        return "DataStorage{" +
                "list=" + list +
                '}';
    }
}
