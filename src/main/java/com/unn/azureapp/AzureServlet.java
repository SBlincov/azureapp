package com.unn.azureapp;

import com.unn.datamodel.DataStorage;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import java.io.IOException;
import java.io.Reader;
import java.io.Writer;

/**
 * Created by feeldruss on 22.03.17.
 */




public class AzureServlet extends HttpServlet {
    @Override
    protected void doPut(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Reader reqReader = req.getReader();
        StringBuilder builder = new StringBuilder();
        int data;
        while((data = reqReader.read())!=-1) {
            builder.append((char)data);
        }
        try {
            ListUpdateObject.put(builder.toString());
            resp.setStatus(HttpServletResponse.SC_OK);
        } catch (JAXBException e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        } finally {
            reqReader.close();
        }
    }



    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Writer respWriter = resp.getWriter();
        try {
            respWriter.write(ListUpdateObject.get());
            resp.setStatus(HttpServletResponse.SC_OK);
        } catch (JAXBException e) {
            resp.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            e.printStackTrace();
        }
    }


}
