package com.unn.azureapp;

import com.unn.datamodel.DataStorage;
import javafx.scene.chart.PieChart;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by feeldruss on 22.03.17.
 */
public abstract class DataAccessObject {
    private static final JAXBContext jaxbContext = initContext();
    private static final File FILE = initFile();
    private static File initFile() {
        File file = new File("list.xml");
        try {
            if(!file.exists())
                file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();

        }
        return file;
    }
    private static JAXBContext initContext() {
        try {
            return JAXBContext.newInstance(DataStorage.class);
        } catch (JAXBException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static DataStorage getList() throws JAXBException {
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        return (DataStorage) unmarshaller.unmarshal(FILE);
    }

    public static void pushList(DataStorage list) throws JAXBException {
        Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.marshal(list,FILE);
    }

    public static void main(String[] args) {
        try {
            System.out.println(getList());
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
